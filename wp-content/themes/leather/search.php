<?php get_header(); 

$title  = get_the_title();
$keys= explode(" ",$s);
$title  = preg_replace('/('.implode('|', $keys) .')/iu','<strong class="search-excerpt">\0</strong>',$title);
?>
	<main class="herd">
		<?php if ( have_posts() ) : ?>
			<h2><h2>Search Results for <mark><?php the_search_query() ?></mark></h2>
</h2>
			<div class="clear">
				<?php while ( have_posts() ) : the_post(); ?>
					<article>
						<?php if ( has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>" class="img"><?php the_post_thumbnail('medium'); ?></a>
						<?php } else { ?>
							<a href="<?php the_permalink(); ?>" class="noimg"></a>
						<?php } ?>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<?php the_excerpt_rereloaded($words = 42, $link_text = 'Olvass tovább'); ?>
					</article>
				<?php endwhile; ?>
			</div>
			<?php if(function_exists('wp_page_numbers')) : wp_page_numbers(); endif; ?>
			<?php else : ?>
		<?php endif; ?>
	</main>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>

