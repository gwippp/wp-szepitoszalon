<?php get_template_part('html-head'); ?>
<div class="middle">
	<header role="banner">
		<h1 class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<h2><?php bloginfo('description'); ?></h2>
		<nav id="main_menu" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
		</nav>
	</header>
