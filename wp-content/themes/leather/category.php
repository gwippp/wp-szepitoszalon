<?php get_header(); ?>
	<main class="herd">
		<?php if ( have_posts() ) : ?>
			<h2><?php single_cat_title(); ?></h2>
			<div class="clear">
				<?php while ( have_posts() ) : the_post(); ?>
					<article>
						<?php global $post; ?>
						<?php if ( has_post_thumbnail() ) { ?>
							<a href="<?php the_permalink(); ?>" class="img"><?php the_post_thumbnail( 'thumbnail' ); ?></a>
						<?php } else { ?>
							<a href="<?php the_permalink(); ?>" class="noimg"></a>
						<?php } ?>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<small class="date"><?php echo date('Y.m.d.', strtotime($post->post_date)); ?></small>
						<?php the_excerpt_rereloaded($words = 25, $link_text = 'Olvass tovább'); ?>
					</article>
				<?php endwhile; ?>
			</div>
			<?php if(function_exists('wp_page_numbers')) : wp_page_numbers(); endif; ?>
			<?php else : ?>
		<?php endif; ?>
	</main>
	<?php get_sidebar(); ?>
<?php get_footer(); ?>

