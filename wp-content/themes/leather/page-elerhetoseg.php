<?php get_header(); ?>
	<main class="single full">
		<div class="half">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="breadcrumbs">
					<?php if(function_exists('bcn_display')) bcn_display(); ?>
				</div>
				<h2><?php the_title(); ?></h2>
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
			<?php endwhile; endif; ?>
			<?php edit_post_link('Módosítás', '<p>', '</p>'); ?>
		</div>
		<div class="half">
			<?php include (TEMPLATEPATH . "/components/map.php"); ?>
		</div>
	</main>
	<?php // get_sidebar(); ?>
<?php get_footer(); ?>
