<?php
$theme = "default";
?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/nivo_slider/themes/<?php echo($theme); ?>/<?php echo($theme); ?>.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/nivo_slider/nivo-slider.css" type="text/css" media="screen" />

<div class="slider-wrapper theme-<?php echo($theme); ?> place">
	<div class="ribbon"></div>
	<div id="slider" class="nivoSlider">
		<?php query_posts(array('post_type' => 'carousel', 'orderby'=>'date','order'=>'DESC', 'posts_per_page'=>6));
			$cnt = 0;
			while ( have_posts() ) : the_post(); ?>
				<?php global $post; $post->url = get_post_meta($post->ID, 'url', true); ?>
					<?php if ( has_post_thumbnail() ) { ?>
						<?php if(!empty($post->url)){ ?>
							<a href="<?php echo $post->url ?>" rel="bookmark">
						<?php } ?>
							<?php the_post_thumbnail(array(1000,1000), array("title"=>"#slider-caption-".get_the_ID())); ?>
						<?php if(!empty($post->url)){ ?>
							</a>
						<?php } ?>
				<?php } ?>
				<!--h3><a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a></h3>-->
				
				<?php
					//the_excerpt_rereloaded('10','Olvass tovább','','div','button middle gray','no')
					$slider_part .= '
						<div id="slider-caption-'.get_the_ID().'" class="nivo-html-caption">
							<h2>
                                                        ' . (!empty($post->url) ? '<a href="'.$post->url.'">' : '') . ''
                                                        .get_the_title().'
                                                        ' . (!empty($post->url) ? '</a>' : '') . '
                                                        </h2>
                                                        ' . (!empty($post->url) ? '<a href="'.$post->url.'">' : '') . ''
								.the_excerpt_rereloaded('10','Olvass tovább','','div','button middle gray','no', false, true).'
							' . (!empty($post->url) ? '</a>' : '') . '
						</div>
					';
					$cnt++;
				?>
			<?php endwhile;
		wp_reset_query(); ?>
	</div>
</div>
<?php echo $slider_part; ?>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.nivo.slider.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript">
	$(window).load(function() {
		$('#slider').nivoSlider({
			effect:"random",
			slices:15,
			boxCols:8,
			boxRows:4,
			animSpeed:700,
			pauseTime:6000,
			startSlide:0,
			directionNav:true,
			directionNavHide:true,
			controlNav:true,
			controlNavThumbs:false,
			controlNavThumbsFromRel:true,
			keyboardNav:true,
			pauseOnHover:true,
			manualAdvance:true,
			captionOpacity: 0.8,
			prevText: 'Előző',
			nextText: 'Következő'
			/* EFFECTS: sliceDown sliceDownLeft sliceUp sliceUpLeft sliceUpDown sliceUpDownLeft fold fade random slideInRight slideInLeft boxRandom boxRain boxRainReverse boxRainGrow boxRainGrowReverse */
		});
	});
</script>
