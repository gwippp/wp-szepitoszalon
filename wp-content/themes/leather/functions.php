<?php

define('NGG_SKIP_LOAD_SCRIPTS', NULL);

/*JQUERY repair*/
if ( !is_admin() ) {
	function changejquery() {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js');
		wp_enqueue_script( 'jquery' );
	}
	add_action('init', 'changejquery');
}

// MENU SETTINGS

// Register Menus
register_nav_menu( 'primary', __( 'Navigation Menu', 'leather' ) );
register_nav_menu( 'sidebar', __( 'Sidebar menu', 'leather' ) );
register_nav_menu( 'footer', __( 'Footer menu', 'leather' ) );
register_nav_menu( 'footer-social', __( 'Footer social menu', 'leather' ) );
//register_nav_menu( 'user', __( 'User menu', 'leather' ) );

// Widget
function widgets_init() {

	register_sidebar( array(
		'name'          => 'sidebar',
		'id'            => 'sidebar_widget',
		'before_widget' => '<section>',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'widgets_init' );

/*
// First/last menu class
function add_first_and_last($output) {
	$output = preg_replace('/class="menu-item/', 'class="first-menu-item menu-item', $output, 1);
	$output = substr_replace($output, 'class="last-menu-item menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
	return $output;
}
add_filter('wp_nav_menu', 'add_first_and_last');
*/
// CONTENT

// Remove gallery inline style, and br-s

add_filter('the_content', 'remove_br_gallery', 11);
function remove_br_gallery($output) {
	return preg_replace('/(<br[^>]*>\s*){2,}/', '<br />', $output);
}

add_filter('use_default_gallery_style', '__return_false');

/*excrept rereloaded*/
function the_excerpt_rereloaded($words = 40, $link_text = 'Continue reading this entry &#187;', $allowed_tags = '', $container = 'p', $atagclass = 'morea', $smileys = 'no', $need_more_button = true , $return = false) {
	global $post;
	
	if ( $allowed_tags == 'all' ) 
		$allowed_tags = '<a>,<i>,<em>,<b>,<strong>,<ul>,<ol>,<li>,<span>,<blockquote>,<img>';
	
	$text = preg_replace('/\[.*\]/', '', strip_tags($post->post_content, $allowed_tags));
	
	$text = explode(' ', $text);
	$tot = count($text);
	
	for ( $i=0; $i<$words; $i++ ) :
		$output .= $text[$i] . ' ';
	endfor;
		
	if ( $smileys == "yes" ) 
		$output = convert_smilies($output);
	
	$data = '<p>'.force_balance_tags($output);
	
	//ha a szöveg hosszabb, mint amennyi szót meg akarunk jeleníteni, akkor '...', különben lezáró </p>
	if ( $i < $tot ) :
		$data .= '...';
	else :
		$data .= '</p>';
	endif;
	
	//if ( $i < $tot ) : <-- mindig kell a "more"
		if ( $container == 'p' || $container == 'div' ) : $data .= '</p>'; endif;
		if ( $container != 'plain' ) : $data .= '<'.$container.' class="more">';
			if ( $container == 'div' ) : $data .= '<p>'; endif;
		endif;
	
		//$need_more_button alapértelmezetten true, tehát kiírja a gombot is
		if ($need_more_button) { 
			$data .= '<a class="'.$atagclass.'" href="'.get_permalink().'" title="'.$link_text.'">'.$link_text.'</a>';
		}
	
		if ( $container == 'div' ) : $data .= '</p>'; endif;
		if ( $container != 'plain' ) : $data .= '</'.$container.'>'; endif;
		if ( $container == 'plain' || $container == 'span' ) : $data .= '</p>'; endif; 
		//endif;
	
	// $return alapértelmezetten false, tehát kiírja a változó tartalmát
	if ($return)
		return $data;
	else
		echo $data;
}

add_theme_support( 'post-thumbnails', array('post', 'page', 'teacher', 'carousel') );

/*
// nextgen megjelenítés sima képekre
add_filter('the_content', 'addlightbox_shutterset_replace', 12);
add_filter('get_comment_text', 'addlightbox_shutterset_replace');
function addlightbox_shutterset_replace ($content)
{   global $post;
	$pattern = "/<a(.*?)href=('|\")([^>]*).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>(.*?)<\/a>/i";
	$replacement = '<a$1href=$2$3.$4$5 class="shutterset"$6>$7</a>';
	$content = preg_replace($pattern, $replacement, $content);
	return $content;
}
*/

function getFuturePostsByCategoryId($categoryId){
    $query = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('future'),
        'orderby' => 'post_date',
        'order' => 'ASC'
        ));
    
    return $query;    
}

function getPostsOnDateByCategoryId($timestamp, $categoryId){
    //parent 485 type
    //parent 486 channel
    $query = new WP_Query( $args = array(
        'date_query' => array(
                                'year' => date('Y', $timestamp),
                                'month' => date('m', $timestamp),
                                'day'   => date('d', $timestamp)
                             ),
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'ASC'
        ));
    
    $sameDateTimeChecker = array();
    
    while($query->have_posts()){
        $query->next_post();        
        $post_categories = wp_get_post_categories($query->post->ID);
        foreach($post_categories as $c){
                $cat = get_category( $c );
                if($cat->parent === 485){
                    $query->post->customCategory = $cat->name;
                }
                if($cat->parent === 486){
                    $query->post->channel = $cat->name;
                }
        }
        $sameDateTimeChecker[] = $query->post->post_date . $query->post->post_title;
        
    }
    
    
    return $query;
}

function getFirstAndLastPostDateInCategory($categoryId){
    
    $intervalArray = array();
    
    $query = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'ASC',
        'posts_per_page' => 1
        ));
    
    
    while($query->have_posts()){
        $intervalArray['firstDate'] = $query->post->post_date;
        $query->next_post();
    }
    
    $query2 = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'DESC',
        'posts_per_page' => 1
        ));
    
    
    while($query2->have_posts()){
        $intervalArray['lastDate'] = $query2->post->post_date;
        $query2->next_post();
    }
    
    return $intervalArray;
    
}


// seasons body class

function getSeasonCssClass() {
  $month = date('n'); // current month number without leading 0
  $season = array(
    1 => 'winter',
    2 => 'winter',
    3 => 'spring',
    4 => 'spring',
    5 => 'spring',
    6 => 'summer',
    7 => 'summer',
    8 => 'summer',
    9 => 'autumn',
    10 => 'autumn',
    11 => 'autumn',
    12 => 'winter',
  );
  return $season[$month];
}

//add_filter( "single_template", "get_custom_post_type_template" ) ;

